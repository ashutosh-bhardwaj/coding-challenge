import { HeaderLayout } from 'layouts';
import Default from 'views/Default';
import App from 'App';
import Register from 'views/Register';
import endPoints from './endpoints';

export default [
  {
    component: App,
    routes: [
      {
        path: endPoints.default, // partial matching can also work for group of Routes
        component: HeaderLayout, // view Layout
        routes: [
          {
            path: endPoints.register, // exact matching
            exact: true,
            component: Register// view component
          },
          {
            path: endPoints.default, // exact matching
            exact: true,
            component: Default// view component
          },
        ],
      },
    ]
  }
];
