import 'bootstrap/dist/css/bootstrap.min.css';
import 'assets/styles/global.scss';
import 'assets/fonts';

import React from 'react';
import ReactDOM from 'react-dom';
import ErrorBoundary from 'components/ErrorBoundary';
import Routes from 'Routes';

ReactDOM.render(
  <ErrorBoundary>
    <Routes />
  </ErrorBoundary>,
  document.getElementById('root')
);
