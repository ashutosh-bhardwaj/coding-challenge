import React from 'react';
import PropTypes from 'prop-types';
import { renderRoutes } from 'react-router-config';
import Header from 'components/Header';
import './HeaderLayout.scss';

export default function HeaderLayout(props) {
  const { route } = props;
  return (
    <div className="HeaderLayout">
      <div className="HeaderLayout__Header">
      <Header>
        <img src="/img/daffo_logo.png" alt="logo" width="auto" height="60px" />
      </Header>
      </div>
      <div className="HeaderLayout__Content">
        {renderRoutes(route.routes)}
      </div>
    </div>
  );
}

HeaderLayout.propTypes = {
  route: PropTypes.object.isRequired,
};
