import DefaultLayout from './DefaultLayout';
import HeaderLayout from './HeaderLayout';

export default DefaultLayout;

export { HeaderLayout, DefaultLayout };
