import React from 'react';
import PropTypes from 'prop-types';
import { renderRoutes } from 'react-router-config';

export default function DefaultLayout(props) {
  const { route } = props;
  return (
    <div>
      {renderRoutes(route.routes)}
    </div>
  );
}

DefaultLayout.propTypes = {
  route: PropTypes.object.isRequired,
};
