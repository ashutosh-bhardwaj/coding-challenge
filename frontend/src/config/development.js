const API = {
  BASE_URL: 'http://172.18.4.19:8000/api', // sample
};

const DEV_TOOLS = {
  enableReduxDevTools: true,
  logError: true
};

module.exports = {
  API,
  DEV_TOOLS
};
