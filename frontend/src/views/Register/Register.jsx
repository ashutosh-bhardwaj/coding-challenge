import React from 'react';
import PropTypes from 'prop-types';
import { registerProfile } from 'api/register';
import './Register.scss';

const initialState = {
  name: '',
  email: '',
  contactNumber: '',
  profile: '',
  experience: '',
  errorMsg: '',
  isErrorOpen: false,
  apiSuccess: false,
  isProcessing: false
};

export default class Register extends React.Component {
  constructor(props) {
    super(props);
    this.state = initialState;
  }

  handleChange = (field, value) => {
    this.setState({ [field]: value });
  }

  handleSubmit = (e) => {
    e.preventDefault();
    const {
      name, email, contactNumber, profile, experience
    } = this.state;

    this.setState({ isProcessing: true });
    registerProfile({
      name, email, contactNumber, profile, experience
    })
      .then((response) => {
        this.setState(initialState);
        if (
          response
          && response.body
          && response.body.message
        ) {
          this.showErrorNotify(response.body.message, true);
        } else {
          this.showErrorNotify('Profile Added.', true);
        }
      })
      .catch((error) => {
        if (
          error.response
          && error.response.status !== 500
          && error.response.body
          && error.response.body.message
        ) {
          this.showErrorNotify(error.response.body.message);
        } else {
          this.showErrorNotify('Something went wrong.');
        }
      });
  }

  showErrorNotify = (message = '', apiSuccess = false) => {
    this.setState({
      errorMsg: message, isErrorOpen: true, apiSuccess, isProcessing: false
    });
  };

  hideErrorNotify = () => {
    this.setState({ errorMsg: '', isErrorOpen: false, apiSuccess: false });
  }

  render() {
    const {
      name, email, contactNumber, profile, experience,
      errorMsg, isErrorOpen, apiSuccess, isProcessing
    } = this.state;
    return (
      <div className="Register">
        {
          isErrorOpen
          && <ErrorBar message={errorMsg} onClose={this.hideErrorNotify} apiSuccess={apiSuccess} />
        }
        <div className="Register__ContentWrap">
          <form onSubmit={this.handleSubmit}>
            <div className="form-row">
              <div className="form-group">
                <label>Name</label>
                <input required type="text" onChange={e => this.handleChange('name', e.target.value)} value={name} className="form-control" id="name" />
              </div>
            </div>

            <div className="form-row">
              <div className="form-group">
                <label>Email</label>
                <input required type="email" value={email} onChange={e => this.handleChange('email', e.target.value)} className="form-control" id="email" />
              </div>
            </div>

            <div className="form-row">
              <div className="form-group">
                <label>Profile</label>
                <input required type="text" value={profile} id="profile" onChange={e => this.handleChange('profile', e.target.value)} className="form-control" />
              </div>
            </div>

            <div className="form-row">
              <div className="form-group">
                <label>Experience</label>
                <input required type="text" placeholder="eg: 1.2" pattern="[+-]?([0-9]*[.])?[0-9]+" value={experience} id="experience" onChange={e => this.handleChange('experience', e.target.value)} className="form-control" />
              </div>
            </div>

            <div className="form-row">
              <div className="form-group">
                <label>Contact Number</label>
                <input minLength="10" maxLength="10" pattern="[0-9]*" required type="text" value={contactNumber} onChange={e => this.handleChange('contactNumber', e.target.value)} className="form-control" id="contact" placeholder="eg: 9996037785" />
              </div>
            </div>

            <div className="form-row">
            <div className="form-group"></div>
              <button
                type="submit"
                className="btn btn-primary btn-block"
                disabled={isProcessing}
                style={{
                  backgroundColor: '#679e5d',
                  borderColor: '#679e5d'
                }}
              >
                Register
              </button>
            </div>
          </form>
        </div>
        <div className="Register_SideImage">
          <img src="/img/daffo_side.svg" width="400px" alt="img" />
        </div>
      </div>
    );
  }
}


function ErrorBar(props) {
  const { message, onClose, apiSuccess } = props;
  return (
    <div className="ErrorBar" style={{ backgroundColor: apiSuccess ? '#679e5d' : '#ff8383' }}>
      <span className="ErrorBar__Message">
        {message}
      </span>
      <span
        role="button"
        className="ErrorBar_Icon"
        tabIndex="0"
        onClick={onClose}
      >
        X
      </span>
    </div>
  );
}

ErrorBar.propTypes = {
  message: PropTypes.object.isRequired,
  onClose: PropTypes.func.isRequired,
  apiSuccess: PropTypes.bool
};
