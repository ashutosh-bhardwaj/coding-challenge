/**
 * returns device type based on 'Mobi' string in user agent
 */
const getDeviceType = () => {
  const ua = navigator.userAgent;
  let deviceType = 'Desktop';
  if (/Mobi/.test(ua)) {
    deviceType = 'mobile';
  }
  if (/Android/i.test(ua) && !/Mobi/.test(ua)) {
    deviceType = 'tablet';
  }
  return deviceType;
};

/**
 * isEmpty- return whether the given value is empty or not
 *
 * @param {*} value
 * @returns {Boolean}
 */
const isEmpty = value => value === null
  || value === undefined
  || (Array.isArray(value) && !value.length)
  || (typeof value === 'object' && !Object.keys(value).length)
  || (typeof value === 'string' && !value.trim().length);

export {
  getDeviceType,
  isEmpty
};
