import React from 'react';
import PropTypes from 'prop-types';
import { renderRoutes } from 'react-router-config';

export default class App extends React.PureComponent {
  constructor(props) {
    super(props);
    window.appHistory = props.history;
  }

  render() {
    const { route } = this.props;
    return (
      <React.Fragment>
        {renderRoutes(route.routes)}
      </React.Fragment>
    );
  }
}

App.propTypes = {
  route: PropTypes.object.isRequired,
  history: PropTypes.object.isRequired
};
