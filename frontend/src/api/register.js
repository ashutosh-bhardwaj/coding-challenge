import apiCall from './apiRequest';

export function registerProfile(info = {}) {
  return apiCall({
    method: 'post',
    endpoint: 'register/',
    payload: info
  });
}

export default {
  registerProfile
};
