import superagent from 'superagent';
import config from 'config';

const { API } = config;

/*
 * @function "call" common method that makes api requests
 * @param {object} "request" stores the request 'method','endpoint', 'payload', 'query',
 * 'token' as keys...'
 */
export default function call({
  method = 'get',
  url,
  endpoint,
  payload,
  query,
  type = 'application/json'
}) {
  const _url = `${API.BASE_URL}/${endpoint}`;

  const request = superagent(method, endpoint ? _url : url)
    .set('Content-Type', type);
    // .set('x-api-key', API.X_API_KEY);

  return request.send(payload).query(query);
}
