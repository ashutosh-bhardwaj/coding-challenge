import React from 'react';
import './Loader.scss';

export default function Loader() {
  return (
    <div className="Loader">
      <span>
        Loading...
      </span>
    </div>
  );
}
