import React from 'react';
import PropTypes from 'prop-types';

import './Header.scss';

export default function Header({
  children,
  containerClass,
}) {
  return (
    <div className={`Header ${containerClass}`} >
      <div className="Header__title">
        {
          children
        }
      </div>
    </div>
  );
}

Header.propTypes = {
  children: PropTypes.oneOfType([
    PropTypes.object,
    PropTypes.array
  ]),
  containerClass: PropTypes.string,
  itemsClass: PropTypes.string
};

Header.defaultProps = {
  children: [],
  containerClass: '',
  itemsClass: ''
};
