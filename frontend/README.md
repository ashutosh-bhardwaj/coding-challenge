### Frontend


## Install Global Dependencies
* Install node, npm

## Install Project Dependencies
* `npm install`


## App config
* Change `BASE_URL` at frontend/src/config/production.js to `[HOST_URL]/api` where `HOST_URL` is the url where web api is been running.

## Create Front Build
* `npm run build-production` <br/>
  Creates the respective build in dir `/dist`

* Copy the `/dist` directory, host it at server of your choice.

