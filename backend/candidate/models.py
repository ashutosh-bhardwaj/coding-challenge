from djongo import models
from datetime import date
# Create your models here.


class Technology(models.Model):

    name = models.CharField(max_length=50)

    class Meta:
        db_table = 'technology'

    def __str__(self):
        return self.name


class Candidate(models.Model):

    name = models.CharField(max_length=255)
    contact_number = models.CharField(max_length=10)
    email = models.EmailField()
    date_of_interview = models.DateField()
    profile = models.CharField(max_length=100, blank=True)
    experience = models.DecimalField(max_digits=4, decimal_places=1, default=0.0)

    class Meta:
        db_table = 'candidate'

    def __str__(self):
        return self.name

    def save(self, *args, **kwargs):
        if self.date_of_interview is None:
            self.date_of_interview = date.today()
        super(Candidate, self).save(*args, **kwargs)