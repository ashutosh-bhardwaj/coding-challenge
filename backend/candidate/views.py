from candidate import models

from datetime import date, timedelta

from rest_framework import views, status
from rest_framework.response import Response

# Create your views here.


class RegisterCandidateView(views.APIView):
    def post(self, request):

        data = request.data
        old_date = date.today() - timedelta(180)
        candidate = (
            models.Candidate.objects.filter(
                name=data["name"],
                contact_number=data["contact_number"],
                email=data["email"],
                date_of_interview__gte=old_date
            )
            .order_by("-date_of_interview")
            .first()
        )

        if candidate:
            return Response(
                {
                    "message": "The candidate is not eligible as he/she "
                    f"has visited on {candidate.date_of_interview}"
                },
                status=status.HTTP_400_BAD_REQUEST,
            )
        candidate = (
            models.Candidate.objects.filter(
                contact_number=data["contact_number"], email=data["email"],
                date_of_interview__gte=old_date
            )
            .order_by("-date_of_interview")
            .first()
        )

        if candidate:
            candidate = models.Candidate.objects.create(
                name=data["name"],
                email=data["email"],
                contact_number=data["contact_number"],
                profile=data["profile"],
                experience=data.get('experience')
            )
            return Response(
                {
                    "message": f"A candidate with same contact number and"
                    f" email appeared on {candidate.date_of_interview}. "
                    "Please check it manually"
                },
                status=status.HTTP_400_BAD_REQUEST,
            )

        candidate = (
            models.Candidate.objects.filter(
                name=data["name"], email=data["email"],
                date_of_interview__gte=old_date
            )
            .order_by("-date_of_interview")
            .first()
        )

        if candidate:
            candidate = models.Candidate.objects.create(
                name=data["name"],
                email=data["email"],
                contact_number=data["contact_number"],
                profile=data["profile"],
                experience=data.get('experience')
            )
            return Response(
                {
                    "message": "A candidate with same name and email appeared"
                    f" on {candidate.date_of_interview}. "
                    "Please check it manually"
                },
                status=status.HTTP_400_BAD_REQUEST,
            )

        candidate = (
            models.Candidate.objects.filter(
                contact_number=data["contact_number"], name=data["name"],
                date_of_interview__gte=old_date
            )
            .order_by("-date_of_interview")
            .first()
        )

        if candidate:
            candidate = models.Candidate.objects.create(
                name=data["name"],
                email=data["email"],
                contact_number=data["contact_number"],
                profile=data["profile"],
                experience=data.get('experience')
            )

            return Response(
                {
                    "message": "A candidate with same name and contact number "
                    f"appeared on {candidate.date_of_interview}. "
                    "Please check it manually"
                },
                status=status.HTTP_400_BAD_REQUEST,
            )

        candidate = models.Candidate.objects.create(
            name=data["name"],
            email=data["email"],
            contact_number=data["contact_number"],
            profile=data["profile"],
            experience=data.get('experience')
        )

        return Response(
            {"message": f"{data['email']} successfully added"},
            status=status.HTTP_201_CREATED
        )
