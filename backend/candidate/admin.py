from django.contrib import admin
from candidate.models import Candidate, Technology

# Register your models here.

class CandidateAdmin(admin.ModelAdmin):
    list_display = ('name', 'email', 'contact_number', 'date_of_interview', 'profile', 'experience')

admin.site.register(Candidate, CandidateAdmin)
