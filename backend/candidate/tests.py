from candidate import models

from django.shortcuts import reverse
from django.test import TestCase

# Create your tests here.


class TestRegistrationView(TestCase):
    def setUp(self):
        self.tech = models.Technology.objects.create(name='test_tech')

    def test_unsuccessful_registration(self):
        candidate = models.Candidate.objects.create(
            name='Test Candidate',
            email='test@email.com',
            contact_number='1234567890',
            profile=self.tech
        )

        response = self.client.post(
            reverse('candidate:register'),
            data={
                'name': candidate.name,
                'email': candidate.email,
                'contact_number': candidate.contact_number
            }
        )

        self.assertAlmostEqual(response.status_code, 400)
