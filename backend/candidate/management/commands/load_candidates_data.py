import csv

from django.core.management.base import BaseCommand

from candidate.models import Candidate

class Command(BaseCommand):
    help = "Load candidates entries to update DB based on a csv file with a header cloumns" \
           " in the sequence Name, Contact Number, Email, Technology, Date Of Interview and Experience. " \
           "The Date Of Interview column should have value in YYYY-MM-DD formatS"

    def add_arguments(self, parser):
        parser.add_argument('--file_path', type=str, help='Path to CSV file having data')

    def handle(self, *args, **options):
        file_path = options['file_path']

        with open(file_path, mode='r') as csv_file:
            csv_reader = csv.reader(csv_file)
            next(csv_reader)        # SKipping first line

            for row in csv_reader:
                self.stdout.write(self.style.SUCCESS(row))
                Candidate.objects.create(
                    name=row[0],
                    contact_number=row[1],
                    email=row[2],
                    profile=row[3],
                    date_of_interview=row[4],
                    experience=row[5] if row[5] else 0.0
                )
                break