from django.urls import path
from candidate.views import RegisterCandidateView

urlpatterns = [
    path('register/', RegisterCandidateView.as_view(), name='register'),
]
