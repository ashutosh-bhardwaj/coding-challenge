## Setup the Python environment 

1. Install Python 3.6 and pip3
2. Install virtualenv via following command `pip3 install virtualenv`
3. Create virtual environment directory and install python libraries  
     * `python3.6 -m virtualenv env`
     * `source env/bin/activate`
     * `pip install -r requirements.txt`

## Install MongoDB

You can read the [installation instruction here](https://docs.mongodb.com/manual/installation/) for various OS platforms

## Django commands

*Note:-* Activate the environment before running any of the following command using `source env/bin/activate`

* python manage.py migrate             (# To update the database schema)
* python manage.py createsuperuser     (# To create admin user)
* python manage.py runserver           (# To start the development server)
* python manage.py load_candidates_data --file_path FILEPATH    (# To load the data from a csv file)

_The CSV file should have name, contact number, email, profile, date and experience in the respective order._

_The date column should have value in YYYY-MM-DD format._

## Configuring the Backend App

### Database Connection

* Open the `SOA/settings.py`
* Go to the key `DATABASES`
* Update the key values as per DB configuration 
    * `NAME`: Database Name
    * `HOST`: MongoDB IP,
    * `PORT`: MongoDB port,
    * `USER`: username,
    * `PASSWORD`: password,
    * `AUTH_SOURCE`: DB name for Authentication (Comment this if leaving user and password empty),

### Django App Setting
* Open the `SOA/settings.py`
* Update the IP or Domain name in `ALLOWED_HOSTS`
* Set `DEBUG` to False

## Deployment
1. Update the PATH variables in gunicorn_start script
2. Give executable permission to gunicorn_start file (`chmod u+x gunicorn_start`)
3. Create systemd or init.d script to execute gunicorn_start file
4. Create a proxy in nginx for the unix socket created by gunicorn_start script